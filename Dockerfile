FROM node:16-alpine3.14 as dependencies
WORKDIR /nextjs-demo
COPY package.json yarn.lock ./
RUN yarn install --frozen-lockfile

FROM node:16-alpine3.14 as builder
WORKDIR /nextjs-demo
COPY . .
COPY --from=dependencies /nextjs-demo/node_modules ./node_modules
RUN yarn build

FROM node:16-alpine3.14 as runner
WORKDIR /nextjs-demo
ENV NODE_ENV production
# If you are using a custom next.config.js file, uncomment this line.
# COPY --from=builder /nextjs-demo/next.config.js ./
COPY --from=builder /nextjs-demo/public ./public
COPY --from=builder /nextjs-demo/.next ./.next
COPY --from=builder /nextjs-demo/node_modules ./node_modules
COPY --from=builder /nextjs-demo/package.json ./package.json

EXPOSE 3000
CMD ["yarn", "start"]